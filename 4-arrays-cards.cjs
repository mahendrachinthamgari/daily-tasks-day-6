const cards = [{ "id": 1, "card_number": "5602221055053843723", "card_type": "china-unionpay", "issue_date": "5/25/2021", "salt": "x6ZHoS0t9vIU", "phone": "339-555-5239" },
{ "id": 2, "card_number": "3547469136425635", "card_type": "jcb", "issue_date": "12/18/2021", "salt": "FVOUIk", "phone": "847-313-1289" },
{ "id": 3, "card_number": "5610480363247475108", "card_type": "china-unionpay", "issue_date": "5/7/2021", "salt": "jBQThr", "phone": "348-326-7873" },
{ "id": 4, "card_number": "374283660946674", "card_type": "americanexpress", "issue_date": "1/13/2021", "salt": "n25JXsxzYr", "phone": "599-331-8099" },
{ "id": 5, "card_number": "67090853951061268", "card_type": "laser", "issue_date": "3/18/2021", "salt": "Yy5rjSJw", "phone": "850-191-9906" },
{ "id": 6, "card_number": "560221984712769463", "card_type": "china-unionpay", "issue_date": "6/29/2021", "salt": "VyyrJbUhV60", "phone": "683-417-5044" },
{ "id": 7, "card_number": "3589433562357794", "card_type": "jcb", "issue_date": "11/16/2021", "salt": "9M3zon", "phone": "634-798-7829" },
{ "id": 8, "card_number": "5602255897698404", "card_type": "china-unionpay", "issue_date": "1/1/2021", "salt": "YIMQMW", "phone": "228-796-2347" },
{ "id": 9, "card_number": "3534352248361143", "card_type": "jcb", "issue_date": "4/28/2021", "salt": "zj8NhPuUe4I", "phone": "228-796-2347" },
{ "id": 10, "card_number": "4026933464803521", "card_type": "visa-electron", "issue_date": "10/1/2021", "salt": "cAsGiHMFTPU", "phone": "372-887-5974" }]




// 1. Find all card numbers whose sum of all the even position digits is odd.

const cardWithEvenPositonSumOdd = cards.filter((card) => {
    let cardNumberArray = card.card_number.split('').reduce((accumulator, number, index) => {
        if (index % 2 == 0) {
            accumulator += Number(number);
        }
        return accumulator;
    }, 0);
    return cardNumberArray % 2 != 0;
});


// 2. Find all cards that were issued before June.

const cardsIssuedBeforeJune = cards.filter((card) => {
    let cardIssueMonth = card.issue_date.split('/');
    return Number(cardIssueMonth[0] < 6);
});

// 3. Assign a new field to each card for CVV where the CVV is a random 3 digit number.


const newCVVAssigned = cards.map((card) => {
    card['CVV'] = Math.round(Math.random() * 1000);
    return card;
});


//4. Add a new field to each card to indicate if the card is valid or not.

const validCheck = cards.map((card) => {
    card['valid_check'] = 'valid';
    return card;
})


//5. Invalidate all cards issued before March.

const invalidateIssueedBeforeMarch = cards.map((card) => {
    let monthChecker = card.issue_date.split('/');
    if (monthChecker[0] < 3) {
        card.valid_check = 'invalid';
    }
    return card;
});


//6. Sort the data into ascending order of issue date.

const sortedData = cards.sort((a, b) => {
    let aMonth = Number(a.issue_date.split('/')[0]);
    let bMonth = Number(b.issue_date.split('/')[0]);
    return aMonth - bMonth;
});


//7. Group the data in such a way that we can identify which cards were assigned in which months.

const groupedData = cards.reduce((accumulator, card) => {
    const monthOrder = [
        'January',
        'February',
        'March',
        'April',
        'May',
        'June',
        'July',
        'august',
        'september',
        'octomber',
        'November',
        'december'
    ];
    if (accumulator.hasOwnProperty(monthOrder[card.issue_date.split('/')[0]-1])) {
        accumulator[monthOrder[card.issue_date.split('/')[0]-1]].push(card.card_number);
    } else {
        accumulator[monthOrder[card.issue_date.split('/')[0]-1]] = [card.card_number];
    }
    return accumulator;
}, {});

console.log(groupedData);